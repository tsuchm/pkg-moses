#!/usr/bin/env perl

use warnings;
use strict;
use lib '/usr/lib/moses/scripts/training';
use LexicalTranslationModel;

if (scalar(@ARGV) < 4) {

    print STDERR $0." source target alignments output_prefix"."\n"

} else {

    my ($SOURCE,$TARGET,$ALIGNMENT,$OUT) = @ARGV;

    &get_lexical($SOURCE,$TARGET,$ALIGNMENT,$OUT,0);

}


